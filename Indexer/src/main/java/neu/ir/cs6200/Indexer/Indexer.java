package neu.ir.cs6200.Indexer;

/**
 * @author ajay subramanya
 */

import java.io.IOException;

public class Indexer {

	private static final String UNI = "1";
	private static final String BI = "2";
	private static final String TRI = "3";

	public static void main(String[] args) throws IOException {
		Index index = new Index(args[0]);
		String mode = args[1];

		if (mode.equals(UNI)) {
			index.buildUniGram();
			index.writeMapToFile(index.getTermFreq(I(UNI)), "uniGram_TF.txt");
			index.writeDocFreqToFile(index.getDocFreq(I(UNI)), "uniGram_DF.txt");
		} else if (mode.equals(BI)) {
			index.buildBiGram();
			index.writeMapToFile(index.getTermFreq(I(BI)), "biGram_TF.txt");
			index.writeDocFreqToFile(index.getDocFreq(I(BI)), "biGram_DF.txt");
		} else if (mode.equals(TRI)) {
			index.buildTriGram();
			index.writeMapToFile(index.getTermFreq(I(TRI)), "triGram_TF.txt");
			index.writeDocFreqToFile(index.getDocFreq(I(TRI)), "triGram_DF.txt");
		}
	}

	/**
	 * 
	 * @param s
	 *            String
	 * @return Integer
	 */
	private static int I(String s) {
		return Integer.parseInt(s);
	}

}
