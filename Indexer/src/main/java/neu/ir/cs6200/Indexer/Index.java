package neu.ir.cs6200.Indexer;

/**
 * @author ajay subramanya
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class Index {

	Map<String, Map<String, Integer>> uniGram;
	Map<String, Map<String, Integer>> biGram;
	Map<String, Map<String, Integer>> triGram;
	Map<String, Integer> docIdNumTokens;

	String folder;

	public Index(String path) {
		this.uniGram = new HashMap<>();
		this.biGram = new HashMap<>();
		this.triGram = new HashMap<>();
		this.docIdNumTokens = new HashMap<>();
		this.folder = path;
	}

	/**
	 * Builds a unigram inverted index
	 * 
	 * @throws IOException
	 */
	public void buildUniGram() throws IOException {
		File folder = new File(this.folder);

		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			ArrayList<String> tokens = new ArrayList<String>();
			File file = listOfFiles[i];
			String fileName = file.getName();
			if (file.isFile() && fileName.endsWith(".txt")) {
				String content = FileUtils.readFileToString(file);
				tokens = getTokensInFile(content);
				int numTokens = tokens.size();
				docIdNumTokens.put(fileName, numTokens);
				for (int j = 0; j < numTokens; j++) {
					String token = tokens.get(j).toLowerCase().trim();
					if (isValid(token)) addToTable(token, fileName, uniGram);
				}
			}
		}

	}

	/**
	 * 
	 * @param content
	 *            the filtered HTML page contents
	 * @return a list of tokens in the given page
	 */
	private ArrayList<String> getTokensInFile(String content) {
		ArrayList<String> tk = new ArrayList<String>();
		String lines[] = content.split("\\r?\\n");
		for (int j = 0; j < lines.length; j++) {
			String[] tkns = lines[j].split(" +");
			for (String t : tkns) {
				tk.add(applyRegex(t));
			}
		}
		return tk;
	}

	/**
	 * builds the bigram inverted index
	 * 
	 * @throws IOException
	 */
	public void buildBiGram() throws IOException {
		File folder = new File(this.folder);

		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			ArrayList<String> tokens = new ArrayList<String>();
			File file = listOfFiles[i];
			String fileName = file.getName();
			if (file.isFile() && fileName.endsWith(".txt")) {
				String content = FileUtils.readFileToString(file);
				tokens = getTokensInFile(content);
				int numTokens = tokens.size();
				for (int j = 0; j < numTokens; j++) {
					if (j + 1 < numTokens) {
						String token = tokens.get(j).toLowerCase().trim();
						String token1 = tokens.get(j + 1).toLowerCase().trim();
						if (isValid(token) && isValid(token1)) {
							addToTable(token + " " + tokens.get(j + 1), fileName, biGram);
						}
					}
				}
			}
		}

	}

	/**
	 * builds the trigram inverted index
	 * 
	 * @throws IOException
	 */
	public void buildTriGram() throws IOException {

		File folder = new File(this.folder);

		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			ArrayList<String> tokens = new ArrayList<String>();
			File file = listOfFiles[i];
			String fileName = file.getName();
			if (file.isFile() && fileName.endsWith(".txt")) {
				String content = FileUtils.readFileToString(file);
				tokens = getTokensInFile(content);
				int numTokens = tokens.size();
				for (int j = 0; j < numTokens; j++) {
					if (j + 2 < numTokens) {
						String token = tokens.get(j).toLowerCase().trim();
						String token1 = tokens.get(j + 1).toLowerCase().trim();
						String token2 = tokens.get(j + 2).toLowerCase().trim();
						if (isValid(token) && isValid(token1) && isValid(token2)) {
							addToTable(token + " " + token1 + " " + token2, fileName, triGram);
						}
					}

				}
			}
		}

	}

	/**
	 * applies a set of regex rules to the given token to validate it
	 * 
	 * @param token
	 *            a string that needs to be validated
	 * @return the token after regex'n it
	 */
	private String applyRegex(String token) {
		if (StringUtils.isNumeric(token)) return token;
		Pattern regex = Pattern.compile("^[0-9]+([,.][0-9]+)?$");
		Matcher matcher = regex.matcher(token);
		if (matcher.find()) return token;
		return token.replaceAll("(?![-])\\p{Punct}", "");
	}

	/**
	 * performs further checks on the data
	 * 
	 * @param token
	 *            the string which needs to be validated
	 * @return
	 */
	private boolean isValid(String token) {
		if (token.startsWith("http") || token.startsWith("https") || token.contains("https") || token.contains("http"))
			return false;
		if (token.equals("-")) return false;
		if (token.equals(",")) return false;
		if (token.trim().isEmpty()) return false;

		return true;
	}

	/**
	 * adds the given token and doc Id to the HashMap after performing minor
	 * checks
	 * 
	 * @param token
	 *            the [uni|bi|tri]gram term
	 * @param docId
	 *            the document in which the token is present
	 * @param index
	 *            [uniGram|biGram|triGram]
	 */
	private void addToTable(String token, String docId, Map<String, Map<String, Integer>> index) {
		if (index.containsKey(token)) {
			if (index.get(token).containsKey(docId)) {
				int tf = index.get(token).get(docId) + 1;
				index.get(token).put(docId, tf);
			} else {
				index.get(token).put(docId, 1);
			}
		} else {
			Map<String, Integer> temp = new HashMap<>();
			temp.put(docId, 1);
			index.put(token, temp);
		}
	}

	/**
	 * 
	 * @param i
	 *            [uni|bi|tri]
	 * @return the term frequency map
	 */
	public Map<String, Integer> getTermFreq(int i) {
		Map<String, Map<String, Integer>> index;
		Map<String, Integer> res = new HashMap<>();
		if (i == 1)
			index = getUniGram();
		else if (i == 2)
			index = getBiGram();
		else
			index = getTriGram();

		for (String key : index.keySet()) {
			res.put(key, getSum(index.get(key)));
		}
		return sortByComparator(res);

	}

	/**
	 * @author http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 * @param unsortMap
	 * @return
	 */
	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {

		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * utility to write map of string and integer to file
	 * 
	 * @param map
	 *            the map that will be written to the file
	 * @param fileName
	 *            the name of the file
	 */
	public void writeMapToFile(Map<String, Integer> map, String fileName) {
		try {
			File file = new File(fileName);
			if (!file.exists()) file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				bw.write("[TERM] : " + entry.getKey() + " [FREQ] : " + entry.getValue() + "\n");
			}
			bw.close();
			System.out.println("wrote to file " + fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 * @param map
	 */
	public void printMap(Map<String, Integer> map) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println("[TERM] : " + entry.getKey() + " [FREQ] : " + entry.getValue());
		}
	}

	/**
	 * sums over all the term frequencies
	 * 
	 * @param map
	 *            HashMap of DocID and term frequencies
	 * @return the sum of all the term frequencies
	 */
	private Integer getSum(Map<String, Integer> map) {
		int sum = 0;
		for (String s : map.keySet()) {
			sum += map.get(s);
		}
		return sum;
	}

	/**
	 * 
	 * @param i
	 *            [uni|bi|tri]
	 * @return the document frequency map
	 */
	public Map<String, ArrayList<String>> getDocFreq(int i) {
		Map<String, Map<String, Integer>> index;
		Map<String, ArrayList<String>> res = new TreeMap<>();
		if (i == 1)
			index = getUniGram();
		else if (i == 2)
			index = getBiGram();
		else
			index = getTriGram();

		for (String term : index.keySet()) {
			ArrayList<String> docIds = new ArrayList<>();
			for (String docId : index.get(term).keySet()) {
				docIds.add(docId);
			}
			res.put(term, docIds);
		}
		return res;
	}

	/**
	 * utility to print the document frequency
	 * 
	 * @param map
	 *            the map that should be printed
	 */
	public void printDocFreq(Map<String, ArrayList<String>> map) {
		for (String term : map.keySet()) {
			String docIds = "";
			for (String docId : map.get(term)) {
				docIds += docId + "\t";
			}
			System.out.println(" [TERM] " + term + " [DOC IDs] " + docIds + " [df] " + map.get(term).size());
		}
	}

	/**
	 * utility to write doc frequency to file
	 * 
	 * @param map
	 *            the map that will be written to file
	 * @param fileName
	 *            the name of the file
	 */
	public void writeDocFreqToFile(Map<String, ArrayList<String>> map, String fileName) {

		try {
			File file = new File(fileName);
			if (!file.exists()) file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (String term : map.keySet()) {
				String docIds = "";
				for (String docId : map.get(term)) {
					docIds += docId + "\t";
				}
				bw.write(" [TERM] " + term + " [DOC IDs] " + docIds + " [df] " + map.get(term).size() + "\n");
			}
			bw.close();
			System.out.println("wrote to file " + fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * utility to write any map of String and Integer to file
	 * 
	 * @param content
	 *            the String contents
	 * @param fileName
	 *            the name of the file
	 */
	public void writeToFile(String content, String fileName) {
		try {
			File file = new File(fileName);
			if (!file.exists()) file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
			System.out.println("wrote to file " + fileName);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<String, Map<String, Integer>> getUniGram() {
		return uniGram;
	}

	public void setUniGram(Map<String, Map<String, Integer>> uniGram) {
		this.uniGram = uniGram;
	}

	public Map<String, Map<String, Integer>> getBiGram() {
		return biGram;
	}

	public void setBiGram(Map<String, Map<String, Integer>> biGram) {
		this.biGram = biGram;
	}

	public Map<String, Map<String, Integer>> getTriGram() {
		return triGram;
	}

	public void setTriGram(Map<String, Map<String, Integer>> triGram) {
		this.triGram = triGram;
	}

	public Map<String, Integer> getDocIdNumTokens() {
		return docIdNumTokens;
	}

	public void setDocIdNumTokens(Map<String, Integer> docIdNumTokens) {
		this.docIdNumTokens = docIdNumTokens;
	}
}
