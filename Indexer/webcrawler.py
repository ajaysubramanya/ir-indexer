# @author  Ajay Subramanya
# @title  Web crawler
# @description  crawls all the pages on the wikipedia starting from the seed
#               and writes all the URL's crawled to a file at the end
# @data-structure  used `collections.OrderedDict()` to store the frointer. 
# 				   OrderedDict = {URL : depth}
# @algorithm  Breath first search
# @libraries  {BeautifulSoup : HTML parsing}

#!/usr/bin/env python
import urllib2
from bs4 import BeautifulSoup
import collections
import time
import re 

# @description  appends a domain if there none else returns the links as is
# @param  link - part of hyper link or a complete hyper link

def appendDomainIfAbsent(link):
    if "http://en.wikipedia.org/" not in link:
        return "http://en.wikipedia.org"+str(link)
    else:
        return link;

# @description  returns the html contents of the link passed 
# @param  link - part of hyper link or a complete hyper link

def requestPage(link):
    domainedLink =  appendDomainIfAbsent(link)
    page = ""
    
    try:
    	page = urllib2.urlopen(domainedLink).read()
    except urllib2.URLError as e:
    	print "failing for link - ", link
    	print "There seems to be some problem with the network as URL lib is failing..."
    	print e

    if(page):
        return  page
    else:
        print "Invalid link provided"
        exit(0)

# @description gets the links in the passed HTML page
# @param page - a HTML page 

def getLinksInPage(page):
    soup = BeautifulSoup(page, 'html.parser')
    links = []
    for link in soup.find_all('a'):
        links.append(link.get('href'))   

    return links


def getTextInPage(page):
	soup = BeautifulSoup(page, 'html.parser')

	# kill all script and style elements
	for script in soup(["script", "style"]):
		script.extract()

	for nav in soup("div", {"id": "mw-navigation"}):
		nav.extract()

	for header in soup("div", {"id": "footer"}):
		header.extract()

	for siteSub in soup("div", {"id": "siteSub"}):
		siteSub.extract()

	for jumpToNav in soup("div", {"id": "jump-to-nav"}):
		jumpToNav.extract()

	for printfooter in soup("div", {"id": "printfooter"}):
		printfooter.extract()

	for nvView in soup("div", {"class": "nv-view"}):
		nvView.extract()

	for sideTable in soup("div", {"class" : "vertical-navbox"}):
		sideTable.extract()

	for navBx in soup("div", {"id":"navbox"}):
		navBx.extract()

	for td in soup.find_all('table'):
		td.extract()

	for img in soup.find_all('img'):
		img.extract()

	

	# get text
	text = soup.get_text()

	# break into lines and remove leading and trailing space on each
	lines = (line.strip() for line in text.splitlines())
	
	# # break multi-headlines into a line each
	chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
	
	# drop blank lines
	text = '\n'.join(chunk for chunk in chunks if chunk)

	return text.encode('utf-8')


# @description validate the link 
# @param link - hyperlink 

def filterLinks(link):
	if link.find(":") > -1:
		return False
	if link == "/wiki/Sustainable_energy":
		return False
	if link == "/wiki/Wikipedia":
		return False
	if link == "/wiki/Main_Page":
		return False
	if link.startswith("/wiki/"):
		return True

# @description to remove any hash params that may be present in the link 
# 			   this is done to avoid duplication of URL's
# @param link hyper reference link  

def removeHash(link):
	hashIndex = link.find("#")
	if hashIndex > -1:
		return link[:hashIndex]
	return link 

# @description validates the links 
# @aparam links - a list of lines to validate

def validateLinks(links):
	validLinks = []
	for aLink in links:
		if aLink and filterLinks(aLink):
			validLinks.append(removeHash(aLink))		
	return validLinks

# @description : termination condition for the program 
# @param crawledLinks - all the links that have been crawled
# @param depth - the depth until which we have crawled 

def stopCrawl(crawledLinks, depth):
	if depth > 5: 
		return True
	if len (crawledLinks) == 1000:
		return True

# @description  returns the key and value of the supplied dictionary
# @param linksToCrawl - the dictionary which contains the URL and depth 

def getKeyValue(linksToCrawl):
	# getting the key , value from the ordered dictionary
	for link, depth in linksToCrawl.iteritems():
		return link, depth
		break

# @description to be polite to the webserver
# be careful what you pass to n
# n =1 , 1 second
def delayBy(n):
    time.sleep(n)


def stripWiki(link):
	if link.startswith("/wiki/"):
		return link[6:]
	return link 

# the crawler starts crawling here
def main():

	# the starting link 
	seed = "/wiki/Sustainable_energy"
	
	# using ordered dict as it preserves the order in which files are added 
	linksToCrawl = collections.OrderedDict()


	graph = {}
	graph[seed] = []

	# adding sead to frointer and setting it depth to 1
	linksToCrawl[seed] = 1
	crawledLinks = collections.OrderedDict()

	currLink = ""
	currDepth = -1

	pattern = re.compile('[\W_]+')
	
	# while we still have links to crawl 
	while len(linksToCrawl) != 0:

		# being polite to the server
		delayBy(1)

		# getting the key , value from the ordered dictionary
		currLink, currDepth = getKeyValue(linksToCrawl)

		# stop crawling if we have reached 1000 crawled URLs or 
		# we have reached a depth of 5. Which ever is earlier 
		if stopCrawl(crawledLinks, currDepth):
			break

		# make a request to get the page from the link
		page = requestPage(currLink)

		fileName = pattern.sub('', currLink.split("/")[2])+".txt"

		with open(fileName, "w") as text_file:
			text_file.write(getTextInPage(page))

		# now you got the page, time to get the links in that page
		linksInPage = getLinksInPage(page)

		# now we need to validate the links that we got from the page
		validLinks = validateLinks(linksInPage)

		# adding the crawled link and its depth to an other dict	
		crawledLinks[currLink] = currDepth 

		print "number of links crawled ", len(crawledLinks)

		# delete the link for which we have got the links 
		del linksToCrawl[currLink]

		# update the dictonary to add new links from link 
		for vLink in validLinks:

			# appending the outlinks of currPage to the graph dictionary 
			# to find the links of all the crawled pages
			if graph.has_key(vLink):
				graph.get(vLink).append(stripWiki(currLink))
			else:
				graph[vLink] = [stripWiki(currLink)]


			# making sure that the key is not already present, dictionary 
			# handles this , but replaces the value as well - which is not desired
			# checking the url in toCrawl and crawled dicts before adding
			if not linksToCrawl.has_key(vLink) and not crawledLinks.has_key(vLink):
				# adding all the child URLs of the link to our ordered dictonary
				linksToCrawl[vLink] = currDepth + 1

		# filtering off the uncrawled links from the graph 
		newGraph = {}
		for cLink in crawledLinks:
			if cLink in graph:
				newGraph[cLink] = graph[cLink]
	
	print "Done!!!"


if __name__ == '__main__':
    main()
